# Bosch
Assignment
Here is the Following detail to Run this code.

Clone the Repository

Need to be create virtualenv

using `virtualenv ~/envOne `# envOne will be environment Name

Then Activate virtual environment using `source ~/envOne/bin/activate`

Then install dependenies using this cmd `pip install -r requirements.txt`

Then run cmd `python manage.py migrate`

Then run cmd `python manage.py runserver`

your project will be available at https://localhost:8000
